//
//  Rule.swift
//  VoIPManager
//
//  Created by Brad Herring on 3/20/16.
//  Copyright © 2016 Bherville. All rights reserved.
//

import UIKit

class Rule: NSObject, NSCoding {
    // MARK: Properties
    var name: String
    var did: DID
    var route: Route
    var failoverRoute: Route?
    
    // MARK: Achriving Paths
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("rules")
    
    // MARK: Types
    struct ProperyKey {
        static let nameKey = "name"
        static let didKey = "did"
        static let routeKey = "route"
        static let failoverRouteKey = "failoverRoute"
    }
    
    init(name: String, did: DID, route: Route, failoverRoute: Route?) {
        // Initialize stored properties.
        self.name = name
        self.did = did
        self.route = route
        self.failoverRoute = failoverRoute
        
        super.init()
    }
    
    // MARK: NSCoding
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: ProperyKey.nameKey)
        aCoder.encodeObject(did, forKey: ProperyKey.didKey)
        aCoder.encodeObject(route, forKey: ProperyKey.routeKey)
        aCoder.encodeObject(failoverRoute, forKey: ProperyKey.failoverRouteKey)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObjectForKey(ProperyKey.nameKey) as! String
        let did = aDecoder.decodeObjectForKey(ProperyKey.didKey) as! DID
        let route = aDecoder.decodeObjectForKey(ProperyKey.routeKey) as! Route
        let failoverRoute = aDecoder.decodeObjectForKey(ProperyKey.failoverRouteKey) as? Route
        
        // Must call designated initilizer
        self.init(name: name, did: did, route: route, failoverRoute: failoverRoute)
    }
}