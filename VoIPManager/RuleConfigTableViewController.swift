//
//  TableViewController.swift
//  VoIPManager
//
//  Created by Brad Herring on 3/19/16.
//  Copyright © 2016 Bherville. All rights reserved.
//

import UIKit

class RuleConfigTableViewController: UITableViewController, UITextFieldDelegate, UINavigationControllerDelegate {
    // MARK: Properties
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var didLabel: UILabel!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var dids = [DID]()
    
    var did: DID?
    var route: Route?
    var failoverRoute: Route?
    var rule: Rule?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Handle the text field’s user input through delegate callbacks.
        nameTextField.delegate = self
        
        if let rule = rule {
            navigationItem.title = rule.name
            nameTextField.text = rule.name
            did   = rule.did
            route = rule.route
            failoverRoute = rule.failoverRoute
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        // Enable the Save button only if the text field has a valid Meal name.
        checkValidRule()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Hide the keyboard
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        // Disable the Save button while editing
        saveButton.enabled = false
    }
    
    func checkValidRule() {
        // Disable the Save button if the rule is invalid.
        let nameText = nameTextField.text ?? ""
        saveButton.enabled = !(nameText.isEmpty || did == nil || route == nil)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        checkValidRule()
        navigationItem.title = textField.text
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
            case 2:
                return 2
            default:
                return 1
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if saveButton === sender {
            rule = Rule(name: nameTextField.text ?? "", did: did!, route: route!, failoverRoute: failoverRoute)
        }
    }
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        // Depending on style of presentation (modal or push), this view controller needs to be dismissed in two different ways.
        
        let isPresentingInAddMealMode = presentingViewController is UINavigationController
        
        if isPresentingInAddMealMode {
            dismissViewControllerAnimated(true, completion: nil)
        }
            
        else {
            navigationController!.popViewControllerAnimated(true)
        }
    }

    
    func selectedDid(sender: AttributeUIButton){
        // Your code when select button is tapped
        self.dismissViewControllerAnimated(true, completion: nil);
        
        // Get the DID if the user selected one.
        if let did = (sender.attribute as? DIDPickerView)?.selectedDID {
            print(did.number)
            self.did = did
            checkValidRule()
        }
    }
    
    func selectedRoute(sender: AttributeUIButton){
        // Your code when select button is tapped
        self.dismissViewControllerAnimated(true, completion: nil);
        
        // Get the Route if the user selected one.
        if let route = (sender.attribute as? RoutePickerView)?.selectedRoute {
            print(route.name)
            self.route = route
            checkValidRule()
        }
    }
    
    func selectedFailoverRoute(sender: AttributeUIButton){
        // Your code when select button is tapped
        self.dismissViewControllerAnimated(true, completion: nil);
        
        // Get the Failover Route if the user selected one.
        if let failoverRoute = (sender.attribute as? RoutePickerView)?.selectedRoute {
            print(failoverRoute.name)
            self.failoverRoute = failoverRoute
            checkValidRule()
        }
    }
    
    func cancelSelection(sender: UIButton){
        print("Cancel");
        self.dismissViewControllerAnimated(true, completion: nil);
        // We dismiss the alert. Here you can add your additional code to execute when cancel is pressed
    }
    
    func showActionSheet (pickerDelegate: UIPickerViewDelegate, pickerDataSource: UIPickerViewDataSource, okButtonAction: Selector, cancelButtonAction: Selector) {
        let title = ""
        let message = "\n\n\n\n\n\n\n\n\n\n";
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .ActionSheet)
        alertController.modalInPopover = true;
        
        
        let pickerFrame: CGRect = CGRectMake(15, 52, alertController.view.frame.size.width - 50, 100) // CGRectMake(left), top, width, height) - left and top are like margins
        let picker: UIPickerView = UIPickerView(frame: pickerFrame)
        
        picker.delegate = pickerDelegate
        picker.dataSource = pickerDataSource
        alertController.view.addSubview(picker)
        
        
        //Create the toolbar view - the view witch will hold our 2 buttons
        let toolFrame = CGRectMake(15, 5, alertController.view.frame.size.width - 50, 45);
        let toolView: UIView = UIView(frame: toolFrame);
        
        //add buttons to the view
        let buttonCancelFrame: CGRect = CGRectMake(0, 7, 100, 30); //size & position of the button as placed on the toolView
        
        //Create the cancel button & set its title
        let buttonCancel: UIButton = UIButton(frame: buttonCancelFrame);
        buttonCancel.setTitle("Cancel", forState: UIControlState.Normal);
        buttonCancel.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal);
        toolView.addSubview(buttonCancel); //add it to the toolView
        
        //Add the target - target, function to call, the event witch will trigger the function call
        buttonCancel.addTarget(self, action: cancelButtonAction, forControlEvents: UIControlEvents.TouchDown);
        
        
        //add buttons to the view
        let buttonOkFrame: CGRect = CGRectMake(170, 7, alertController.view.frame.size.width - 125, 30); //size & position of the button as placed on the toolView
        
        //Create the Select button & set the title
        let buttonOk: AttributeUIButton = AttributeUIButton(frame: buttonOkFrame);
        buttonOk.setTitle("Select", forState: UIControlState.Normal);
        buttonOk.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal);
        toolView.addSubview(buttonOk); //add to the subview
        
        buttonOk.attribute = pickerDelegate
        buttonOk.addTarget(self, action: okButtonAction, forControlEvents: UIControlEvents.TouchDown);
        
        //add the toolbar to the alert controller
        alertController.view.addSubview(toolView);
        
        self.presentViewController(alertController, animated: true, completion: nil);
    }
    
    // MARK: Actions
    @IBAction func selectDidFromPicker(sender: UITapGestureRecognizer) {
        // Hide the keyboard.
        nameTextField.resignFirstResponder()
        
        let didPicker = DIDPickerView()
        didPicker.dids = [DID(number: "555-555-5555"), DID(number: "555-555-5556")]
        
        showActionSheet(didPicker, pickerDataSource: didPicker, okButtonAction: "selectedDid:", cancelButtonAction: "cancelSelection:")
    }
    
    @IBAction func selectRouteFromPicker(sender: UITapGestureRecognizer) {
        // Hide the keyboard.
        nameTextField.resignFirstResponder()
        
        let routePicker = RoutePickerView()
        routePicker.routes = [Route(name: "Home")!, Route(name: "Office")!]
        
        showActionSheet(routePicker, pickerDataSource: routePicker, okButtonAction: "selectedRoute:", cancelButtonAction: "cancelSelection:")
    }
    
    @IBAction func selectFailoverRouteFromPicker(sender: UITapGestureRecognizer) {
        // Hide the keyboard.
        nameTextField.resignFirstResponder()
        
        let routePicker = RoutePickerView()
        routePicker.routes = [Route(name: "Home")!, Route(name: "Office")!]
        
        showActionSheet(routePicker, pickerDataSource: routePicker, okButtonAction: "selectedFailoverRoute:", cancelButtonAction: "cancelSelection:")
    }
}
