//
//  RuleTableViewController.swift
//  VoIPManager
//
//  Created by Brad Herring on 3/20/16.
//  Copyright © 2016 Bherville. All rights reserved.
//

import UIKit

class RuleTableViewController: UITableViewController {
    // MARK: Properties
    var rules = [Rule]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        
        if let savedRules = loadRules() {
            rules += savedRules
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rules.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "RuleTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! RuleTableViewCell
        
        // Fetches the appropriate meal for the data source layout.
        let rule = rules[indexPath.row]
        
        cell.nameLabel.text = rule.name
        cell.didLabel.text = rule.did.number
        
        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            rules.removeAtIndex(indexPath.row)
            
            // Save the rules.
            saveRules()
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowDetail" {
            let ruleConfigTableViewController = segue.destinationViewController as! RuleConfigTableViewController
            
            // Get the cell that generated this segue.
            if let selectedRuleCell = sender as? RuleTableViewCell {
                let indexPath = tableView.indexPathForCell(selectedRuleCell)!
                let selectedRule = rules[indexPath.row]
                ruleConfigTableViewController.rule = selectedRule
            }
        }
            
        else if segue.identifier == "AddItem" {
            print("Adding new rule.")
        }
    }

    
    // MARK: Actions
    @IBAction func unwindToRuleList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.sourceViewController as? RuleConfigTableViewController, rule = sourceViewController.rule {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                // Update an existing rule.
                rules[selectedIndexPath.row] = rule
                tableView.reloadRowsAtIndexPaths([selectedIndexPath], withRowAnimation: .None)
            }
                
            else {
                // Add a new rule.
                print(rules.count)
                let newIndexPath = NSIndexPath(forRow: rules.count, inSection: 0)
                rules.append(rule)
                tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Bottom)
            }
            
            // Save the rules.
            saveRules()
        }
    }

    // MARK: NSCoding
    func saveRules() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(rules, toFile: Rule.ArchiveURL.path!)
        if !isSuccessfulSave {
            print("Failed to save rules...")
        }
    }
    
    func loadRules() -> [Rule]? {
        return NSKeyedUnarchiver.unarchiveObjectWithFile(Rule.ArchiveURL.path!) as? [Rule]
    }
}
