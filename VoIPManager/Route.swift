//
//  Route.swift
//  VoIPManager
//
//  Created by Brad Herring on 3/20/16.
//  Copyright © 2016 Bherville. All rights reserved.
//

import UIKit

class Route: NSObject, NSCoding {
    // MARK: Properties
    var name: String
    
    // MARK: Achriving Paths
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("routes")
    
    // MARK: Types
    struct ProperyKey {
        static let nameKey = "name"
    }
    
    init?(name: String) {
        // Initialize stored properties.
        self.name = name
        
        super.init()
        
        if name.isEmpty {
            return nil
        }
    }
    
    // MARK: NSCoding
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: ProperyKey.nameKey)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObjectForKey(ProperyKey.nameKey) as! String
        
        // Must call designated initilizer
        self.init(name: name)
    }
}