//
//  RoutePickerView.swift
//  VoIPManager
//
//  Created by Brad Herring on 3/20/16.
//  Copyright © 2016 Bherville. All rights reserved.
//

import UIKit

class RoutePickerView: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    // MARK: Properties
    var routes = [Route]()
    var selectedRoute: Route?
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return routes.count
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        selectedRoute = routes[row]
        return routes[row].name
    }
}