//
//  DID.swift
//  VoIPManager
//
//  Created by Brad Herring on 3/20/16.
//  Copyright © 2016 Bherville. All rights reserved.
//

import UIKit

class DID: NSObject, NSCoding {
    // MARK: Properties
    var number: String
    
    // MARK: Achriving Paths
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("dids")
    
    // MARK: Types
    struct ProperyKey {
        static let numberKey = "number"
    }
    
    init(number: String) {
        self.number = number
    }
    
    // MARK: NSCoding
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(number, forKey: ProperyKey.numberKey)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let number = aDecoder.decodeObjectForKey(ProperyKey.numberKey) as! String
        
        // Must call designated initilizer
        self.init(number: number)
    }
}
