//
//  DIDPickerView.swift
//  VoIPManager
//
//  Created by Brad Herring on 3/20/16.
//  Copyright © 2016 Bherville. All rights reserved.
//

import UIKit

class DIDPickerView: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    var dids = [DID]()
    var selectedDID: DID?
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dids.count
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        selectedDID = dids[row]
        return dids[row].number
    }
}
